using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariasClases
{
    class Hombre
    {
        string nombre;
        int edad;

        public void afeitandose()
        {
            Console.WriteLine("El hombre afeitandose");
        }
        public void damenombre(string nom)
        {
            nombre = nom;
        }
        public string devuelvenombre()
        {
            return nombre;
        }
    }
}
