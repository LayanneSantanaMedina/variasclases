using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariasClases
{
    class Program
    {
        static void Main(string[] args)
        {
            Mujer m1 = new Mujer();
            Mujer m2 = new Mujer();
            Hombre h1 = new Hombre();
            Hombre h2 = new Hombre();

            //llamo el metodo creado en Mujer.cs
            m1.maquillandose();
            h1.afeitandose();
            m1.damenombre("Hillary");
            Console.WriteLine("El nombre de ella es " + m1.devuelvenombre());
            m2.damenombre("Rosa");

            h2.damenombre("Domingo");
            Console.WriteLine("El nombre de el es " + h2.devuelvenombre());

            Console.Read();
        }
    }
}
